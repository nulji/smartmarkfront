import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallbackApiComponent } from './callback-api.component';

describe('CallbackApiComponent', () => {
  let component: CallbackApiComponent;
  let fixture: ComponentFixture<CallbackApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallbackApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallbackApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
