import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/providers/post.service';

@Component({
  selector: 'app-callback-api',
  templateUrl: './callback-api.component.html',
  styleUrls: ['./callback-api.component.scss']
})
export class CallbackApiComponent implements OnInit {
  posts: any;

  constructor(private postService: PostService) { }

  ngOnInit() {
  }

  callApi() {
    this.postService.getPosts().subscribe(
      data => this.posts = data,
      error => console.error('error', error)
    );
  }

}
