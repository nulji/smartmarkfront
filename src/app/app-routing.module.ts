import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartPageComponent } from './components/chart-page/chart-page.component';
import { CallbackApiComponent } from './components/callback-api/callback-api.component';
import { ToolboxComponent } from './components/toolbox/toolbox.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'chart',
    pathMatch: 'full'
  },
  { path: 'chart', component: ChartPageComponent },
  { path: 'callback-api', component: CallbackApiComponent },
  { path: 'toolbox', component: ToolboxComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
