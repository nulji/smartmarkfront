import { Component, OnInit } from '@angular/core';
import { ConfigService } from './providers/config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'smartmark-front';
  devmode: boolean;

  constructor(private configService: ConfigService) { }

  ngOnInit() {
    this.devmode = this.configService.isDevmode();
  }
}
