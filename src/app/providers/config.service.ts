import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor() { }

  isDevmode() {
    return !environment.production;
  }

  getApi(key: string): string {
    return environment.API_ENDPOINTS[key];
  }

  get(key: any) {
    return environment[key];
  }
}
