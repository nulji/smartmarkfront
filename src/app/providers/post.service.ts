import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { IPost } from '../models/IPost';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private url: string;

  constructor(private http: HttpClient, private configService: ConfigService) {
    this.url = this.configService.getApi('POSTS');
  }

  getPosts() {
    return this.http.get<IPost[]>(this.url);
  }
}
