export const environment = {
  production: true,
  environmentName: 'PROD',
  API_ENDPOINTS: {
    POSTS: 'https://jsonplaceholder.typicode.com/posts'
  }
};
