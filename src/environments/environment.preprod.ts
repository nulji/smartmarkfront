export const environment = {
    production: true,
    environmentName: 'PREPROD',
    API_ENDPOINTS: {
      POSTS: 'https://jsonplaceholder.typicode.com/posts'
    }
};
